package remote

const (
	ModelFactoryModelVersionUrl = "/api/v1/studioModelVersion/%d/%d"
	ModelCheckUrl               = "/api/v1/studioCheckInferenceModel"
	ModelTransformUrl           = "/api/v1/studioTransformInferenceModel"
	ModelInferInfoUrl           = "/api/v1/studioModelVersionInference/%d/%d"

	APFLOW_URL        = "/api/v1/apflow/service"
	AomGetResourceApi = "/api/v1/nodes-info/get-system-resource"
	FileServerApi     = "/api/v1/files/loc"
)
