harborAddr = harbor.apulis.cn:8443
tag = v0.1.0

run:
	# cd cmd; go build -buildmode=pie -o ../bin/bff-apflow -v; strip ../bin/bff-apflow; ../bin/bff-apflow;
	go run ./main/apflow.go 

get-deps:
	git submodule init
	git submodule update

image:
	docker build . -f ./dockerfile/dockerfile -t $(harborAddr)/huawei630/apulistech/bff-apflow-backend:$(tag)
	docker push $(harborAddr)/huawei630/apulistech/bff-apflow-backend:$(tag)

vet-check-all: get-deps
	go vet ./...

gosec-check-all: get-deps
	gosec ./...

image-pause:
	docker build . -f ./dockerfile/dockerfile -t $(harborAddr)/huawei630/apulistech/bff-apflow-backend:pause
	docker push $(harborAddr)/huawei630/apulistech/bff-apflow-backend:pause