// Copyright 2020 Apulis Technology Inc. All rights reserved.

package configs

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var Config AppConfig

type AppConfig struct {
	DebugModel bool
	Portal     PortalConfig
	Log        LogConfig
	Db         DbConfig
	Rabbitmq   Rabbitmq
	Relevant   Relevant
}

type Rabbitmq struct {
	Host         string
	Port         string
	UserName     string
	Password     string
	Vhost        string
	ExchangeName string
}

type Base struct {
	ModId string
	Url   string
}

type DbConfig struct {
	Username     string
	Password     string
	Host         string
	Port         int
	Database     string
	Sslmode      string
	MaxOpenConns int
	MaxIdleConns int
}

type HttpConfig struct {
	Address string
	Port    int
	Enable  bool
}

type PortalConfig struct {
	NodeCheckerInterval        int32
	ApplicationCheckerInterval int32
	Http                       HttpConfig
}

type LogConfig struct {
	Level     logrus.Level
	WriteFile bool
	FileDir   string
	FileName  string
}

type Relevant struct {
	ModelFactorySvc             string
	InferenceBackendSvc         string
	AomSvc                      string
	ModelMaxNumPerTask          int64
	ResourceLabel               string
	ModeId                      string
	FileServerSvc               string
	MqSubMod                    string
	NpuInferImage               string
	NpuInferImageTag            string
	GpuInferTransformerImage    string
	GpuInferTransformerImageTag string
	Jwt                         Jwt
}

type Jwt struct {
	SignAlgorithm string
	SecretKey     string
	PublicKey     string
}

func InitConfig(configFile string, config *AppConfig) {
	viper.SetConfigFile(configFile)

	// set default
	viper.SetDefault("DebugModel", false)

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error read config file: %s ", err))
	}

	if err := viper.Unmarshal(&config); err != nil {
		panic(fmt.Errorf("fatal error unmarshal config file: %s ", err))
	}
	config.Relevant.ModelFactorySvc = "apworkshop-backend"

	fmt.Printf("Portal config = %+v\n", config.Portal)
	fmt.Printf("Db config = %+v\n", config.Db)
	fmt.Printf("Rabbitmq config = %+v\n", config.Rabbitmq)
	fmt.Printf("RelevantSvc config = %+v\n", config.Relevant)
}
