package utils

import (
	"io/ioutil"
	"log"
	"time"

	"gopkg.in/yaml.v2"
)

func GetHttpUrl(host string, path string) string {
	return "http://" + host + path
}

// 毫秒
func TimeToTimestamp(t *time.Time) int64 {
	return t.UnixNano() / 1e6
}

func TimestampToTime(t int64) time.Time {
	return time.Unix(t/1000, 0)
}

func ReadYamlToMap(file string) (map[string]interface{}, error) {
	res := make(map[string]interface{})

	yamlFile, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}

	err = yaml.Unmarshal(yamlFile, res)
	if err != nil {
		return nil, err
	}
	log.Println("yaml to map: \n", res)
	return res, nil
}
