package protocol

type ModelInfo struct {
	InferenceInfo InferInfo `json:"inferenceInfo"`
}

type InferInfo struct {
	Name string  `json:"name"`
	Tags TagInfo `json:"tags"`
}

type TagInfo struct {
	FilterLabel string `json:"filterLabel"`
}

type ModelVersionInfo struct {
	StoragePath string `json:"storagePath"`
}

// 模型是否需要转换接口
type ModelCheckReq struct {
	ModelId        int64  `json:"modelId"`
	ModelVersionId int64  `json:"modelVersionId"`
	DeviceType     string `json:"deviceType"`
}

type ModelCheckRsp struct {
	NeedTransform bool `json:"needTransform"`
	CanInference  bool `json:"canInference"`
}

// 模型转换
type ModelTransformReq struct {
	ServiceId      string `json:"serviceId"`
	ModelId        int64  `json:"modelId"`
	ModelVersionId int64  `json:"modelVersionId"`
	DeviceType     string `json:"deviceType"`
	QueueName      string `json:"queueName"`
}

type ModelTransformRsp struct{}

// 模型infer信息
type ModelInferRsp struct {
	// InputParams  []InputParam  `json:"inputParams"`
	// OutputParams []OutputParam `json:"outputParams"`
	// modelVersionInference `json:"center"`
	Center map[string]interface{} `json:"center"`
}

type modelVersionInference struct {
	Center Params `json:"center"`
}

type Params struct {
	InputParams  []InputParam  `json:"inputParams"`
	OutputParams []OutputParam `json:"outputParams"`
}

type InputParam struct {
	Name     string `json:"name"`
	Type     string `json:"type"`
	Required string `json:"required"`
	Desc     string `json:"desc"`
}

type OutputParam struct {
	Name string `json:"name"`
	Type string `json:"type"`
	Desc string `json:"desc"`
}
