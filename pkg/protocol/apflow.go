package protocol

// 创建 apflow
type CreatApflowServiceReq struct {
	Name       string        `json:"name"`
	Caller     string        `json:"caller"`
	ServiceId  string        `json:"serviceId"`
	Replicas   int32         `json:"replicas"`
	ImageFile  string        `json:"imageFile"`
	Envs       string        `json:"envs"`
	DeviceType string        `json:"deviceType"`
	Arch       string        `json:"arch"`
	Cpu        float64       `json:"cpu"`
	Mem        int64         `json:"mem"`
	TypeSpec   string        `json:"typeSpec"`
	DeviceNum  int64         `json:"deviceNum"`
	Models     []ApflowModel `json:"models"`
}

type ApflowModel struct {
	Name      string `json:"name" validate:"required"`
	ID        int64  `json:"id" validate:"required"`
	PvcPath   string `json:"pvcPath" validate:"required"`
	Version   string `json:"version" validate:"required"`
	VersionId int64  `json:"versionId" validate:"required"`
	// FilterLabel string `json:"filterLabel" validate:"required"`
}

type CreatApflowServiceRsp struct {
	Url       string `json:"url"`
	ServiceId string `json:"serviceId"`
}

// 删除
type DeleteServiceReq struct {
	ServiceId string `json:"serviceId"`
}
type DeleteServiceRsp struct{}
