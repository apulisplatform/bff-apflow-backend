package protocol

type UserInfoAAA struct {
	UserId   int    `header:"userId"`
	UserName string `header:"userName"`

	GroupId   int    `header:"groupId"`
	GroupName string `header:"groupAccount"`

	OrgId   int    `header:"orgId"`
	OrgName string `header:"orgName"`

	Authorization string `header:"authorization"`
}
