package common

import "errors"

const (
	SUCCESS_CODE         = 0
	NOT_FOUND_ERROR_CODE = 100200001
	UNKNOWN_ERROR_CODE   = 100200002
	SERVER_ERROR_CODE    = 100200003
	// Request error codes
	PARAMETER_ERROR_CODE  = 100200004
	AUTH_ERROR_CODE       = 100200005
	NAME_CHECK_ERROR_CODE = 100200006
	// APP error codes
	APP_ERROR_CODE = 100200007

	// error
	ErrUnsupportCmdCode       = 100200100
	ErrModelTooManyCode       = 100200101
	ErrModelEmptyCode         = 100200102
	ErrStatusDeleteCode       = 100200103
	ErrModelNotCanInferCode   = 100200104
	ErrModelCheckCode         = 100200105
	ErrModelTransformCode     = 100200106
	ErrInitEndpointPolicyCode = 100200107
)

var (
	ErrUnsupportCmd       = errors.New("err unsupport cmd")
	ErrModelTooMany       = errors.New("model is too many")
	ErrModelEmpty         = errors.New("model is empty")
	ErrStatusDelete       = errors.New("status not allow to delete")
	ErrModelCanNotInfer   = errors.New("model can't infer")
	ErrModelCheck         = errors.New("model check error")
	ErrModelTransform     = errors.New("model transform error")
	ErrInitEndpointPolicy = errors.New("init endpoint policy error")
)

var ErrorMap = map[error]int{
	ErrUnsupportCmd:       ErrUnsupportCmdCode,
	ErrModelTooMany:       ErrModelEmptyCode,
	ErrModelEmpty:         ErrModelEmptyCode,
	ErrStatusDelete:       ErrStatusDeleteCode,
	ErrModelCanNotInfer:   ErrModelNotCanInferCode,
	ErrModelCheck:         ErrModelCheckCode,
	ErrModelTransform:     ErrModelTransformCode,
	ErrInitEndpointPolicy: ErrInitEndpointPolicyCode,
}
