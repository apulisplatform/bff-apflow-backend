package common

const (
	TASK_STATUS_MODEL_TRANSFORM        = "model-transform"
	TASK_STATUS_MODEL_TRANSFORM_OK     = "model-transform-ok"
	TASK_STATUS_MODEL_TRANSFORM_FAILED = "model-transform-failed"
	TASK_STATUS_SCHEDULING             = "scheduling"
	TASK_STATUS_RUNNING                = "running"
	TASK_STATUS_STOP                   = "stop"
	TASK_STATUS_ERROR                  = "error"
	TASK_STATUS_TERMINATING            = "terminating"
)

var StatusEnToZh = map[string]string{
	TASK_STATUS_MODEL_TRANSFORM:        "模型转换中",
	TASK_STATUS_MODEL_TRANSFORM_OK:     "模型转换成功",
	TASK_STATUS_MODEL_TRANSFORM_FAILED: "模型转换失败",
	TASK_STATUS_SCHEDULING:             "调度中",
	TASK_STATUS_RUNNING:                "运行中",
	TASK_STATUS_STOP:                   "停止",
	TASK_STATUS_ERROR:                  "错误",
	TASK_STATUS_TERMINATING:            "停止中",
}

const (
	CMD_START = "start"
	CMD_STOP  = "stop"
)

const (
	PATH_AIPLATFORM_MODEL_DATA_PVC = "/models" // 容器内models挂载路径
)

const (
	DeviceTypeGpu = "GPU"
	DeviceTypeNpu = "NPU"
	DeviceTypeCpu = "CPU"
)

const (
	PredictHost = "%s.default.dev.aistudio.apulis"
)

// 服务不可删除的状态
var NotDelSvcStatus = map[string]string{
	TASK_STATUS_SCHEDULING:  "scheduling",
	TASK_STATUS_RUNNING:     "running",
	TASK_STATUS_TERMINATING: "terminating",
}
