package dao

import (
	"fmt"
	"strings"
)

func ParseGetParams(s string) map[string]string {
	// str="name|desc,age|desc"
	sortMap := make(map[string]string)
	if s == "" {
		return sortMap
	}
	sortList := strings.Split(s, ",")
	for _, val := range sortList {
		i := strings.Split(val, "|")
		if len(i) != 2 {
			fmt.Println("bad params: ", s)
			return sortMap
		}
		if i[0] == "createdAt" {
			i[0] = "created_at"
		}
		sortMap[i[0]] = i[1]
	}
	return sortMap
}

func ParseSort(s string) map[string]string {
	sortMap := ParseGetParams(s)
	if _, ok := sortMap["created_at"]; !ok {
		sortMap["created_at"] = "desc"
	}
	return sortMap
}
