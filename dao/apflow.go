package dao

import (
	"apflow/pkg/protocol"
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
)

const (
	TableInferenceTask string = "t_bffapflow_task"
)

const ()

type ApflowTask struct {
	ID             int64      `gorm:"primary_key" json:"id"`
	OrgId          int64      `gorm:"not null" json:"orgId"`
	GroupId        int64      `gorm:"not null" json:"groupId"`
	UserId         int64      `gorm:"not null" json:"userId"`
	ServiceId      string     `gorm:"describe:'服务id'" json:"serviceId"`
	UserName       string     `gorm:"not null;describe:'创建人'" json:"userName"`
	Name           string     `gorm:"unique;size:255;not null;describe:'任务名称'" json:"name"`
	Describe       string     `gorm:"size:255;describe:'任务描述'" json:"describe"`
	Replicas       int32      `gorm:"describe:'副本数'" json:"replicas"`
	Caller         string     `gorm:"describe:'启动者'" json:"caller"`
	StartTime      int64      `gorm:"describe:'启动时间'" json:"startTime"`
	Url            string     `gorm:"describe:'推理url'" json:"url"`
	Status         string     `gorm:"describe:'服务状态'" json:"status"`
	ReplicasStatus string     `gorm:"describe:'副本状态'" json:"replicasStatus"`
	TaskType       string     `gorm:"describe:'服务类型'" json:"taskType"`
	Events         string     `gorm:"describe:'服务事件'" json:"events"`
	Arch           string     `gorm:"describe:'机器架构'" json:"arch"`
	DeviceType     string     `gorm:"describe:'设备类型'" json:"deviceType"`
	DeviceNum      int64      `gorm:"describe:'设备数量'" json:"deviceNum"`
	Cpu            float64    `gorm:"not null;describe:'cpu'" json:"cpu"`
	Mem            int64      `gorm:"not null;describe:'内存'" json:"mem"`
	TypeSpec       string     `gorm:"not null;describe:'资源型号'" json:"typeSpec"`
	Models         string     `gorm:"describe:'模型'" json:"models"`
	Envs           string     `gorm:"describe:'环境变量'" json:"envs"`
	CreatedAt      *time.Time `gorm:"notnull" json:"createdTime"`
	UpdatedAt      *time.Time `gorm:"notnull" json:"updatedTime"`
	DeletedAt      *time.Time `gorm:"default null" json:"deletedAt"`
}

type ApflowModel struct {
	Name      string `json:"name"`
	ID        int64  `json:"id"`
	PvcPath   string `json:"pvcPath"`
	Version   string `json:"version"`
	VersionId int64  `json:"versionId"`
}

func (ApflowTask) TableName() string {
	return TableInferenceTask
}

func (t *ApflowTask) Save(tx *gorm.DB) error {
	return tx.Save(t).Error
}

func (t *ApflowTask) GetList(tx *gorm.DB, pageNum int64, pageSize int64, taskType string, name string, sort string, status string, userInfo *protocol.UserInfoAAA) ([]ApflowTask, int64, error) {
	var list []ApflowTask
	var temp []ApflowTask
	var err error
	var count int64

	offset := (pageNum - 1) * pageSize
	query := tx.Table(t.TableName())

	// 排序
	sortMap := ParseSort(sort)
	fmt.Printf("sortMap: %s \n", sortMap)
	for k, v := range sortMap {
		order := fmt.Sprintf("%s %s", k, v)
		query = query.Order(order)
	}

	// 按 name 模糊搜索
	if name != "" {
		name = "%" + name + "%"
		query = query.Where("name LIKE ?", name)
	}
	// 按组织筛选
	query = query.Where("group_id = ?", userInfo.GroupId)

	// 按条件筛选
	if status != "" {
		fmt.Println("status:", len(status))
		query = query.Where("status = ?", status)
	}

	if taskType == "all" {
		errCount := query.Find(&temp).Count(&count).Error
		if errCount != nil {
			return nil, 0, errCount
		}
		err := query.Limit(pageSize).Offset(offset).Find(&list).Error
		if err != nil {
			return nil, 0, err
		}
	} else {
		errCount := query.Find(&temp).Count(&count).Error
		if errCount != nil {
			return nil, 0, errCount
		}
		err := query.Limit(pageSize).Offset(offset).Find(&list).Error
		if err != nil {
			return nil, 0, err
		}
	}
	return list, count, err
}

func (t *ApflowTask) GetById(tx *gorm.DB) (*ApflowTask, error) {
	var info ApflowTask

	err := tx.Where("id = ?", t.ID).Find(&info).Error
	if err != nil {
		return nil, err
	}
	return &info, err
}

func (t *ApflowTask) GetByServiceId(tx *gorm.DB) (*ApflowTask, error) {
	var info ApflowTask

	err := tx.Where("service_id = ?", t.ServiceId).Find(&info).Error
	if err != nil {
		return nil, err
	}
	return &info, err
}

func (t *ApflowTask) UpdateById(tx *gorm.DB, data interface{}) error {

	err := tx.Model(&ApflowTask{}).Where("id = ?", t.ID).Updates(data).Error
	if err != nil {
		return err
	}
	return nil
}

func (t *ApflowTask) UpdateStatusByServiceId(tx *gorm.DB) error {

	err := tx.Model(&ApflowTask{}).Where("service_id = ?", t.ServiceId).Updates(map[string]interface{}{
		"status":          t.Status,
		"replicas_status": t.ReplicasStatus,
		"events":          t.Events,
	}).Error

	if err != nil {
		return err
	}
	return nil
}

func (t *ApflowTask) UpdateModelStatusByServiceId(tx *gorm.DB) error {

	err := tx.Model(&ApflowTask{}).Where("service_id = ?", t.ServiceId).Updates(map[string]interface{}{
		"status": t.Status,
	}).Error

	if err != nil {
		return err
	}
	return nil
}

func (t *ApflowTask) UpdateCallerById(tx *gorm.DB) error {

	err := tx.Model(&ApflowTask{}).Where("id = ?", t.ID).Update("caller", t.Caller).Error
	if err != nil {
		return err
	}
	return nil
}

func (t *ApflowTask) DeleteById(tx *gorm.DB) error {
	return tx.Where("id = ?", t.ID).Unscoped().Delete(&ApflowTask{}).Error
}

func (t *ApflowTask) UpdateStatusById(tx *gorm.DB) error {
	return tx.Model(&ApflowTask{}).Where("id = ?", t.ID).Update("status", t.Status).Error
}

func (t *ApflowTask) GetInferenceList(tx *gorm.DB, pageNum int64, pageSize int64, jobStatus string, orgId int64, userGroupId int64,
	jobId string, projectName string, UserName string, sort string, filter string, startTime int64, endTime int64) ([]ApflowTask, int64, error) {

	var list []ApflowTask
	var temp []ApflowTask
	var err error
	var count int64

	offset := (pageNum - 1) * pageSize

	query := tx.Table(t.TableName())

	// //降序
	// if timeDesc {
	// 	orderFiled += " DESC"
	// }
	// name = "%" + name + "%"

	filterMap := make(map[string]interface{})
	if jobStatus != "" {
		filterMap["status"] = jobStatus
	}
	if orgId != 0 {
		filterMap["org_id"] = orgId
	}
	if userGroupId != 0 {
		filterMap["user_groupId"] = userGroupId
	}
	if jobId != "" {
		filterMap["service_id"] = jobId
	}
	if projectName != "" {
		filterMap["name"] = projectName
	}
	if UserName != "" {
		filterMap["user_name"] = UserName
	}

	// 排序
	orderFiled := "created_at"
	if sort != "asc" {
		orderFiled += " DESC"
	}

	query = query.Where(filterMap)
	if startTime > 0 && endTime < 0 {
		query = query.Where("start_time BETWEEN ? AND ?", startTime, endTime)
	}

	errCount := query.Find(&temp).Count(&count).Error
	if errCount != nil {
		return nil, 0, errCount
	}

	err = query.Limit(pageSize).Offset(offset).Order(orderFiled).Find(&list).Error
	if err != nil {
		return nil, 0, err
	}

	return list, count, err
}
