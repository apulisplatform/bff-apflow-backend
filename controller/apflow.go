package controller

import (
	apiserver "apflow/apiserver"
	"apflow/dto"
	"apflow/pkg/common"
	"fmt"
	"strconv"

	"github.com/gin-gonic/gin"
)

func InferenceTaskRouter(r *gin.Engine) {
	group := r.Group("/api/v1/bffapflow")

	group.GET("/task", dto.Wrapper(GetInferenceTaskList))
	group.GET("/task/:taskId", dto.Wrapper(GetInferenceTaskDetail))
	group.POST("/task", dto.Wrapper(CreateInferenceTask))
	group.PUT("/task/:taskId", dto.Wrapper(PutInferenceTask))
	group.DELETE("/task/:taskId", dto.Wrapper(DelInferenceTask))
	group.PATCH("/task/:taskId", dto.Wrapper(OpInferenceTask))
	group.GET("/task/log/download/:taskId", dto.Wrapper(LogDownload))
	group.GET("/task/predict_info/:taskId", dto.Wrapper(PredictInfo))
	group.GET("/task/list", dto.Wrapper(APSCInferenceTaskList))
	group.GET("/task/static_info", dto.Wrapper(StaticInfo))
	// group.POST("/task/predict", func(c *gin.Context) {
	// 	predictUrl := c.Request.Header.Get("predictUrl")
	// 	predictHost := c.Request.Header.Get("predictHost")
	// 	predictDeviceType := c.Request.Header.Get("predictDeviceType")

	// 	c.Request.Header.Add("", "")
	// 	for key, value := range c.Keys {
	// 		c.Request.Header.Set(key, fmt.Sprintf("%+v", value))
	// 		fmt.Printf("%s:%v\n", key, value)
	// 	}

	// 	u, _ := url.Parse(predictUrl)
	// 	setHeadersWithContextKeys(c)
	// 	proxy := proxys.GetOrCreatePatternProxy(u)
	// 	proxy.ServeHTTP(c.Writer, c.Request)
	// })
}

func CreateInferenceTask(c *gin.Context) error {
	var err error
	var req dto.CreateInferenceTaskReq
	var rsp dto.CreateInferenceTaskRsp

	err = c.ShouldBindJSON(&req)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}
	userInfo, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}

	taskId, err := apiserver.CreateInferenceTask(req, userInfo)
	if err != nil {
		logger.Errorln(err)
		return dto.ErrorToCodeRsp(c, err)
	}

	rsp = dto.CreateInferenceTaskRsp{
		ID: taskId,
	}
	return dto.SuccessResp(c, rsp)
}

func GetInferenceTaskList(c *gin.Context) error {
	var err error
	var req dto.GetInferenceTaskListReq
	var rsp dto.GetInferenceTaskListRsp
	var list []dto.OutApflowTaskItem
	var count int64

	err = c.ShouldBindQuery(&req)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}
	userInfo, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}

	list, count, err = apiserver.GetInferenceTaskList(req, userInfo)
	if err != nil {
		return dto.ErrorToCodeRsp(c, err)
	}

	rsp = dto.GetInferenceTaskListRsp{
		Items: list,
		Total: count,
	}
	return dto.SuccessResp(c, rsp)
}

func GetInferenceTaskDetail(c *gin.Context) error {
	var err error
	var req dto.GetInferenceTaskDetailReq
	var rsp dto.GetInferenceTaskDetailRsp
	var idStr string
	var id int64

	idStr = c.Param("taskId")
	id, err = strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}

	_, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}
	out, err := apiserver.GetInferenceTaskDetail(id)
	if err != nil {
		return dto.ErrorToCodeRsp(c, err)
	}

	rsp = dto.GetInferenceTaskDetailRsp{
		OutApflowTaskItem: *out,
	}
	return dto.SuccessResp(c, rsp)
}

func PutInferenceTask(c *gin.Context) error {
	var err error
	var req dto.PutInferenceTaskReq
	var rsp dto.PutInferenceTaskRsp
	var idStr string
	var id int64

	idStr = c.Param("taskId")
	id, err = strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}

	err = c.ShouldBindJSON(&req)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}
	userInfo, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}

	err = apiserver.PutInferenceTask(req, id, userInfo)
	if err != nil {
		return dto.ErrorToCodeRsp(c, err)
	}

	return dto.SuccessResp(c, rsp)
}

func DelInferenceTask(c *gin.Context) error {
	var err error
	var req dto.DelInferenceTaskReq
	var rsp dto.DelInferenceTaskRsp
	var idStr string
	var id int64

	idStr = c.Param("taskId")
	id, err = strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}

	_, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}

	err = apiserver.DelInferenceTask(id)
	if err != nil {
		return dto.ErrorToCodeRsp(c, err)
	}

	return dto.SuccessResp(c, rsp)
}

func OpInferenceTask(c *gin.Context) error {
	var err error
	var req dto.OpInferenceTaskReq
	var rsp dto.OpInferenceTaskRsp
	var idStr string
	var id int64

	idStr = c.Param("taskId")
	id, err = strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}

	err = c.ShouldBindJSON(&req)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}
	userInfo, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}

	err = apiserver.OpInferenceTask(req, id, userInfo)
	if err != nil {
		return dto.ErrorToCodeRsp(c, err)
	}

	return dto.SuccessResp(c, rsp)
}

func LogDownload(c *gin.Context) error {
	var req dto.GetLogDownloadReq
	var rsp dto.GetLogDownloadRsp

	taskId := c.Param("taskId")
	id, err := strconv.ParseInt(taskId, 10, 64)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}
	fmt.Println("task id:", taskId)
	jobId, err := apiserver.GetServiceIdById(id)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}

	_, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}
	downloadlink, err := apiserver.DownloadLogs(jobId)
	if err != nil {
		return dto.ErrorToCodeRsp(c, err)
	}
	rsp = dto.GetLogDownloadRsp{
		Downloadlink: downloadlink,
	}
	return dto.SuccessResp(c, rsp)
}

func PredictInfo(c *gin.Context) error {
	var req dto.PredictInfoReq
	var rsp *dto.PredictInfoRsp

	idStr := c.Param("taskId")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}

	_, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}

	rsp, err = apiserver.PredictInfo(id)
	if err != nil {
		return dto.ErrorToCodeRsp(c, err)
	}

	return dto.SuccessResp(c, rsp)
}

func APSCInferenceTaskList(c *gin.Context) error {
	var err error
	var req dto.APSCInferenceTaskListReq
	var rsp dto.APSCInferenceTaskListRsp
	var list []dto.APSCOutApflowTaskItem
	var count int64

	err = c.ShouldBindQuery(&req)
	if err != nil {
		return dto.ParameterError(c, err.Error())
	}
	userInfo, errRsp := PreHandler(c, &req)
	if errRsp != nil {
		return errRsp
	}

	list, count, err = apiserver.APSCInferenceTaskList(req, userInfo)
	if err != nil {
		return dto.ErrorToCodeRsp(c, err)
	}

	rsp = dto.APSCInferenceTaskListRsp{
		Items:    list,
		PageSize: req.PageSize,
		PageNum:  req.PageNum,
		Total:    count,
	}
	return dto.SuccessResp(c, rsp)
}

func StaticInfo(c *gin.Context) error {
	var rsp dto.APSCStaticInfoRsp

	for k, v := range common.StatusEnToZh {
		rsp.TaskStatus = append(rsp.TaskStatus, dto.APSCTaskStatus{
			Value: k,
			Label: v,
		})
		rsp.TaskTypes = append(rsp.TaskTypes, dto.APSCTaskType{
			Value: "inference",
			Label: "推理",
		})
	}
	return dto.SuccessResp(c, rsp)
}

func Predict(c *gin.Context) error {
	// var err error
	// var req dto.CreateInferenceTaskReq
	// var rsp dto.CreateInferenceTaskRsp

	// err = c.ShouldBindJSON(&req)
	// if err != nil {
	// 	return dto.ParameterError(c, err.Error())
	// }
	// userInfo, errRsp := PreHandler(c, &req)
	// if errRsp != nil {
	// 	return errRsp
	// }

	// taskId, err := apiserver.CreateInferenceTask(req, userInfo)
	// if err != nil {
	// 	logger.Errorln(err)
	// 	return dto.ErrorToCodeRsp(c, err)
	// }

	// rsp = dto.CreateInferenceTaskRsp{
	// 	ID: taskId,
	// }
	// proxyUrl := c.Request.Header.Get("proxyUrl")
	// predictHost := c.Request.Header.Get("predictHost")

	// u, _ := url.Parse(proxyUrl)
	// proxy := httputil.NewSingleHostReverseProxy(u)

	// proxy.ServeHTTP(c.Writer, c.Request)

	// proxy := &httputil.ReverseProxy{Director: director, Transport: transport}

	return dto.SuccessResp(c, "rsp")
}

func setHeadersWithContextKeys(c *gin.Context) {
	for key, value := range c.Keys {
		c.Request.Header.Set(key, fmt.Sprintf("%+v", value))
		fmt.Printf("%s:%v\n", key, value)
	}
}

// func ReverseProxyWithPattern() gin.HandlerFunc {
// 	return func(c *gin.Context) {
// 		setHeadersWithContextKeys(c)
// 		proxy := proxys.GetOrCreatePatternProxy(scheme, host, currentUrlPrefix, realUrlPrefix)
// 		proxy.ServeHTTP(c.Writer, c.Request)
// 	}
// }
