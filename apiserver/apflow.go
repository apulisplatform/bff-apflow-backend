package aipserver

import (
	"apflow/dao"
	"apflow/dto"
	"apflow/pkg/common"
	"apflow/pkg/database"
	"apflow/pkg/loggers"
	"apflow/pkg/protocol"
	"apflow/remote"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/jinzhu/gorm"
)

var logger = loggers.LogInstance()

func CreateInferenceTask(req dto.CreateInferenceTaskReq, userInfo *protocol.UserInfoAAA) (int64, error) {
	var err error
	// 检查模型是否为空
	if CheckIsModelsEmpty(req.Models) {
		return 0, common.ErrModelEmpty
	}
	// 检查模型数是否超出限制
	if CheckIsModelNumOver(req.Models) {
		return 0, common.ErrModelTooMany
	}

	serviceId := GenServiceId()
	// 模型转换 TODO
	apModels, canStart, err := ModelInfo(req, serviceId)
	if err != nil {
		return 0, err
	}

	modelsStr, err := json.Marshal(apModels)
	if err != nil {
		return 0, err
	}

	envsStr, err := json.Marshal(req.Envs)
	if err != nil {
		return 0, err
	}

	tx := database.Db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	now := time.Now()
	t := &dao.ApflowTask{
		OrgId:      int64(userInfo.OrgId),
		GroupId:    int64(userInfo.GroupId),
		UserId:     int64(userInfo.UserId),
		ServiceId:  serviceId,
		UserName:   userInfo.UserName,
		Name:       req.Name,
		Describe:   req.Describe,
		Replicas:   req.Replicas,
		Caller:     userInfo.UserName,
		Status:     common.TASK_STATUS_MODEL_TRANSFORM,
		TaskType:   req.TaskType,
		Arch:       req.Resource.Arch,
		DeviceType: req.DeviceType,
		DeviceNum:  req.Resource.DeviceNum,
		Cpu:        req.Resource.Cpu,
		Mem:        req.Resource.Mem,
		TypeSpec:   req.Resource.TypeSpec,
		Models:     string(modelsStr),
		Envs:       string(envsStr),
		CreatedAt:  &now,
		UpdatedAt:  &now,
	}

	if err = t.Save(tx); err != nil {
		tx.Rollback()
		return 0, err
	}
	// 调用inference-backen创建推理
	if canStart {
		err = StartApflow(t, userInfo, tx)
		if err != nil {
			tx.Rollback()
			return 0, err
		}
	}
	return t.ID, tx.Commit().Error
}

func GetInferenceTaskList(req dto.GetInferenceTaskListReq, userInfo *protocol.UserInfoAAA) ([]dto.OutApflowTaskItem, int64, error) {
	var err error
	var items []dao.ApflowTask
	var count int64
	var t dao.ApflowTask

	tx := database.Db
	items, count, err = t.GetList(tx, req.PageNum, req.PageSize, req.TaskType, req.Name, req.Sort, req.Status, userInfo)
	if err != nil {
		return nil, 0, err
	}
	outPutList := []dto.OutApflowTaskItem{}
	for _, item := range items {
		outItem, err := GenOutApflowTaskItem(item)
		if err != nil {
			return nil, 0, err
		}
		outPutList = append(outPutList, outItem)
	}
	return outPutList, count, err
}

func APSCInferenceTaskList(req dto.APSCInferenceTaskListReq, userInfo *protocol.UserInfoAAA) ([]dto.APSCOutApflowTaskItem, int64, error) {
	var err error
	var items []dao.ApflowTask
	var count int64
	var t dao.ApflowTask

	tx := database.Db
	items, count, err = t.GetInferenceList(tx, req.PageNum, req.PageSize, req.JobStatus, int64(userInfo.OrgId), int64(userInfo.GroupId),
		req.JobId, req.ProjectName, req.UserName, req.Sort, req.Filter, req.StartTime, req.EndTime)
	if err != nil {
		return nil, 0, err
	}
	outPutList := []dto.APSCOutApflowTaskItem{}
	for _, item := range items {
		outItem, err := GenAPSCOutApflowTaskItem(item)
		if err != nil {
			return nil, 0, err
		}
		outPutList = append(outPutList, outItem)
	}
	return outPutList, count, err
}

func GetInferenceTaskDetail(id int64) (*dto.OutApflowTaskItem, error) {
	var err error
	var tx *gorm.DB
	var item *dao.ApflowTask
	var outItem dto.OutApflowTaskItem

	tx = database.Db
	t := &dao.ApflowTask{
		ID: id,
	}
	item, err = t.GetById(tx)
	if err != nil {
		return nil, err
	}
	outItem, err = GenOutApflowTaskItem(*item)
	if err != nil {
		return nil, err
	}
	return &outItem, nil
}

func PutInferenceTask(req dto.PutInferenceTaskReq, id int64, userInfo *protocol.UserInfoAAA) error {
	var tx *gorm.DB
	var err error
	// 检查模型是否为空
	if CheckIsModelsEmpty(req.Models) {
		return common.ErrModelEmpty
	}

	// 检查模型数 <= 16
	if CheckIsModelNumOver(req.Models) {
		return common.ErrModelTooMany
	}

	tx = database.Db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	updateTaskMap, err := GenEditDataMap(req)
	if err != nil {
		tx.Rollback()
		return err
	}

	t := dao.ApflowTask{
		ID: id,
	}
	err = t.UpdateById(tx, updateTaskMap)
	if err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit().Error
}

func DelInferenceTask(id int64) error {

	tx := database.Db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	t := dao.ApflowTask{
		ID: id,
	}
	item, err := t.GetById(tx)
	if err != nil {
		return err
	}
	if _, ok := common.NotDelSvcStatus[item.Status]; ok {
		return common.ErrStatusDelete
	}
	err = t.DeleteById(tx)
	if err != nil {
		return err
	}
	return tx.Commit().Error
}

func DownloadLogs(jobId string) (string, error) {

	// 获取路径
	objectName := jobId + strconv.FormatInt(time.Now().Unix(), 10) + ".tar.gz"
	getFileLocResp, err := remote.GetFileLocation(objectName)
	if err != nil {
		return "", err
	}
	targetFilePath := "/model/" + strings.TrimPrefix(getFileLocResp.Location, fmt.Sprintf("pvc://%s/", "aiplatform-app-data-pvc"))
	fmt.Println("target file: ", targetFilePath)

	resourcePath := GetAiartsPodInferenceJobLogDir(jobId)
	fmt.Println("resource dir: ", resourcePath)
	err = CheckPathExists(resourcePath)
	if err != nil {
		return "", errors.New("日志不存在，请先启动任务！")
	}

	err = CompressFile(resourcePath, targetFilePath)
	if err != nil {
		return "", err
	} else {
		fmt.Printf("压缩文件（%+v）成功， 目标文件（%+v）\n", resourcePath, resourcePath)
	}
	return getFileLocResp.DownloadLink, nil
}

func OpInferenceTask(req dto.OpInferenceTaskReq, id int64, userInfo *protocol.UserInfoAAA) error {
	var err error

	tx := database.Db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	t := &dao.ApflowTask{
		ID: id,
	}
	curTask, err := t.GetById(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	if req.Cmd == common.CMD_START {
		// 启动
		err = StartApflow(curTask, userInfo, tx)
		if err != nil {
			tx.Rollback()
			return err
		}

	} else if req.Cmd == common.CMD_STOP {
		// 停止
		err = remote.DeleteApflow(curTask.ServiceId, curTask.DeviceType)
		if err != nil {
			tx.Rollback()
			return err
		}
		err = UpdateTaskStopStatus(curTask, tx)
		if err != nil {
			tx.Rollback()
			return err
		}
	} else {
		tx.Rollback()
		return common.ErrUnsupportCmd
	}
	return tx.Commit().Error
}

func PredictInfo(id int64) (*dto.PredictInfoRsp, error) {

	tx := database.Db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()
	t := dao.ApflowTask{
		ID: id,
	}
	item, err := t.GetById(tx)
	if err != nil {
		return nil, err
	}
	tx.Commit()
	apModels := []dao.ApflowModel{}
	err = json.Unmarshal([]byte(item.Models), &apModels)
	if err != nil {
		return nil, err
	}
	apModel := apModels[0]
	rspContent, err := remote.GetModelInferInfoformApWorkShop(apModel.ID, apModel.VersionId)
	if err != nil {
		return nil, err
	}

	predictHost := ""
	proxyUrl := ""
	if item.DeviceType == common.DeviceTypeGpu {
		predictHost = fmt.Sprintf(common.PredictHost, item.ServiceId)
		proxyUrl = "/aipredict/api/v1/predict"
	}
	rsp := &dto.PredictInfoRsp{
		Center:      rspContent.Center,
		PredictHost: predictHost,
		Status:      item.Status,
		Url:         item.Url,
		ProxyUrl:    proxyUrl,
	}
	return rsp, nil
}
