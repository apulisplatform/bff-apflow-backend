package aipserver

import (
	"apflow/dao"
	"apflow/dto"
	"apflow/pkg/common"
	"apflow/pkg/configs"
	"apflow/pkg/database"
	"apflow/pkg/protocol"
	"apflow/pkg/utils"
	"apflow/remote"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
)

func CheckIsModelsEmpty(models []dto.Model) bool {
	return len(models) == 0
}

func CheckIsModelNumOver(models []dto.Model) bool {
	return len(models) > int(configs.Config.Relevant.ModelMaxNumPerTask)
}

func GenServiceId() string {
	timeN := time.Now().UnixNano()
	return fmt.Sprintf("af-%s", strconv.FormatInt(timeN, 10))
}

func GetAiartsPodInferenceJobLogDir(jobId string) string {
	return fmt.Sprintf("/model/model/inference/jobdata/%s/logs", jobId)
}

func CheckPathExists(path string) error {
	_, err := os.Stat(path)
	return err
}

func CompressFile(path string, tPath string) error {

	cmd := exec.Command("tar", "-zcf", tPath, path)
	if _, err := cmd.Output(); err != nil {
		logger.Error(fmt.Sprintf("run cmd %s error: %s", cmd.String(), err.Error()))
		return err
	}

	return nil
}

func GenOutApflowTaskItem(item dao.ApflowTask) (dto.OutApflowTaskItem, error) {
	var m []dto.Model

	err := json.Unmarshal([]byte(item.Models), &m)
	if err != nil {
		return dto.OutApflowTaskItem{}, err
	}
	out := dto.OutApflowTaskItem{
		ID:             item.ID,
		OrgId:          item.OrgId,
		GroupId:        item.GroupId,
		UserId:         item.UserId,
		ServiceId:      item.ServiceId,
		UserName:       item.UserName,
		Name:           item.Name,
		Describe:       item.Describe,
		Replicas:       item.Replicas,
		Caller:         item.Caller,
		StartTime:      item.StartTime,
		Url:            item.Url,
		Status:         item.Status,
		ReplicasStatus: item.ReplicasStatus,
		TaskType:       item.TaskType,
		Events:         item.Events,
		Arch:           item.Arch,
		DeviceType:     item.DeviceType,
		DeviceNum:      item.DeviceNum,
		Cpu:            item.Cpu,
		Mem:            item.Mem,
		TypeSpec:       item.TypeSpec,
		Models:         m,
		Envs:           item.Envs,
		CreatedAt:      utils.TimeToTimestamp(item.CreatedAt),
		UpdatedAt:      utils.TimeToTimestamp(item.UpdatedAt),
	}
	if item.StartTime != 0 {
		now := time.Now()
		out.RunTime = utils.TimeToTimestamp(&now) - item.StartTime
	} else {
		out.RunTime = 0
	}
	return out, nil
}

func GenAPSCOutApflowTaskItem(item dao.ApflowTask) (dto.APSCOutApflowTaskItem, error) {
	var m []dto.Model

	err := json.Unmarshal([]byte(item.Models), &m)
	if err != nil {
		return dto.APSCOutApflowTaskItem{}, err
	}
	out := dto.APSCOutApflowTaskItem{
		ID:          item.ID,
		Status:      item.Status,
		Type:        "推理",
		ProjectName: item.Name,
		OwnerUser:   "",
		OwnerOrg:    "",
		UserGroup:   "",
		CreatedAt:   utils.TimeToTimestamp(item.CreatedAt),
		Resources: []dto.Resource{
			{
				Model:  item.TypeSpec,
				Count:  item.DeviceNum,
				Amount: 1,
			},
		},
	}
	if item.StartTime != 0 {
		now := time.Now()
		out.Duration = utils.TimeToTimestamp(&now) - item.StartTime
	} else {
		out.Duration = 0
	}
	return out, nil
}

func GenCreateApflowReq(t dao.ApflowTask, userInfo *protocol.UserInfoAAA) (protocol.CreatApflowServiceReq, error) {
	var models []protocol.ApflowModel
	var req protocol.CreatApflowServiceReq
	var imageFileName string

	err := json.Unmarshal([]byte(t.Models), &models)
	if err != nil {
		return req, err
	}

	if t.DeviceType == common.DeviceTypeNpu {
		imageFileName = fmt.Sprintf("%s:%s", configs.Config.Relevant.NpuInferImage, configs.Config.Relevant.NpuInferImageTag)
	} else {
		imageFileName = fmt.Sprintf("%s:%s", configs.Config.Relevant.GpuInferTransformerImage, configs.Config.Relevant.GpuInferTransformerImageTag)
	}

	req = protocol.CreatApflowServiceReq{
		Name: t.Name,
		// Caller:     userInfo.UserName,
		Caller:     "test",
		ServiceId:  t.ServiceId,
		Replicas:   t.Replicas,
		ImageFile:  imageFileName,
		Envs:       t.Envs,
		DeviceType: t.DeviceType,
		Arch:       t.Arch,
		Cpu:        t.Cpu,
		Mem:        t.Mem,
		TypeSpec:   t.TypeSpec,
		DeviceNum:  t.DeviceNum,
		Models:     models,
	}
	return req, nil
}

func GenEditDataMap(req dto.PutInferenceTaskReq) (map[string]interface{}, error) {
	modelsStr, err := json.Marshal(req.Models)
	if err != nil {
		return nil, err
	}
	envsStr, err := json.Marshal(req.Envs)
	if err != nil {
		return nil, err
	}

	updateTaskMap := make(map[string]interface{})
	updateTaskMap["name"] = req.Name
	updateTaskMap["taskType"] = req.TaskType
	updateTaskMap["describe"] = req.Describe
	updateTaskMap["models"] = modelsStr
	updateTaskMap["deviceType"] = req.DeviceType
	updateTaskMap["replicas"] = req.Replicas
	updateTaskMap["envs"] = envsStr
	updateTaskMap["arch"] = req.Resource.Arch
	updateTaskMap["cpu"] = req.Resource.Cpu
	updateTaskMap["mem"] = req.Resource.Mem
	updateTaskMap["typeSpec"] = req.Resource.TypeSpec
	updateTaskMap["deviceNum"] = req.Resource.DeviceNum
	return updateTaskMap, nil
}

func StartApflow(t *dao.ApflowTask, userInfo *protocol.UserInfoAAA, tx *gorm.DB) error {
	// 调用inference-backen创建推理
	logger.Infoln("Start Apflow")
	createApflowReq, err := GenCreateApflowReq(*t, userInfo)
	if err != nil {
		return err
	}
	creatApflowRsp, err := remote.CreatApflow(createApflowReq)
	if err != nil {
		return err
	}
	updateMap := make(map[string]interface{})
	updateMap["service_id"] = creatApflowRsp.ServiceId
	updateMap["url"] = creatApflowRsp.Url
	updateMap["startTime"] = time.Now().UnixNano() / 1e6
	updateMap["caller"] = userInfo.UserName
	updateMap["status"] = common.TASK_STATUS_SCHEDULING
	err = t.UpdateById(tx, updateMap)
	if err != nil {
		return err
	}
	return nil
}

func UpdateTaskStopStatus(t *dao.ApflowTask, tx *gorm.DB) error {
	var err error

	updateMap := make(map[string]interface{})
	updateMap["url"] = ""
	updateMap["status"] = common.TASK_STATUS_TERMINATING // 状态变为停止中
	updateMap["start_time"] = 0
	err = t.UpdateById(tx, updateMap)
	if err != nil {
		return err
	}
	return nil
}

func GetServiceIdById(id int64) (string, error) {
	var serviceId string

	tx := database.Db
	t := dao.ApflowTask{
		ID: id,
	}

	task, err := t.GetById(tx)
	if err != nil {
		return serviceId, err
	}
	if task.ServiceId == "" {
		return serviceId, errors.New("serviceId is None")
	}
	serviceId = task.ServiceId
	logger.Infoln("service id: ", serviceId)
	return serviceId, nil
}

// 获取模型信息
func ModelInfo(req dto.CreateInferenceTaskReq, serviceId string) ([]dao.ApflowModel, bool, error) {
	var apModels []dao.ApflowModel

	canStart, err := ModelCheckAndTransform(req, serviceId)
	if err != nil {
		return nil, canStart, err
	}
	for _, m := range req.Models {
		modelPath, err := remote.GetModelVersionDetailsFromApWorkShop(m.ID, m.VersionId)
		if err != nil {
			return nil, canStart, err
		}
		apModels = append(apModels, dao.ApflowModel{
			Name:      m.Name,
			ID:        m.ID,
			Version:   m.Version,
			VersionId: m.VersionId,
			PvcPath:   modelPath.StoragePath,
		})
	}
	return apModels, canStart, nil
}

// 模型检查和转换
func ModelCheckAndTransform(req dto.CreateInferenceTaskReq, serviceId string) (bool, error) {

	for _, m := range req.Models {
		// 检查
		if req.DeviceType == common.DeviceTypeCpu {
			req.Resource.TypeSpec = "nvidia_gpu"
		}
		modelCheck, err := remote.ModelCheckFromApWorkShop(m.ID, m.VersionId, req.Resource.TypeSpec)
		if err != nil {
			logger.Errorln(err)
			return false, common.ErrModelCheck
		}
		// 不能使用
		if !modelCheck.CanInference {
			return false, common.ErrModelCanNotInfer
		}
		// 不需要转换
		if !modelCheck.NeedTransform {
			return true, nil
		} else {
			// 需要转换
			_, err = remote.ModelTransformApWorkShop(m.ID, m.VersionId, req.Resource.TypeSpec, serviceId, "centre_inference_notify")
			if err != nil {
				logger.Errorln(err)
				return false, common.ErrModelTransform
			}
		}
	}
	return false, nil
}
