package uuid

type IdGenerator interface {
	Generate() int64
}
