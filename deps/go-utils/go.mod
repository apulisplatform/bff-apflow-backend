module github.com/apulis/sdk/go-utils

go 1.16

require (
	github.com/Shopify/sarama v1.19.0
	github.com/Shopify/toxiproxy v2.1.4+incompatible // indirect
	github.com/aws/aws-sdk-go-v2 v1.5.0
	github.com/aws/aws-sdk-go-v2/config v1.2.0
	github.com/aws/aws-sdk-go-v2/service/s3 v1.7.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/eapache/go-resiliency v1.2.0 // indirect
	github.com/eapache/go-xerial-snappy v0.0.0-20180814174437-776d5712da21 // indirect
	github.com/eapache/queue v1.1.0 // indirect
	github.com/frankban/quicktest v1.13.0 // indirect
	github.com/golang/protobuf v1.4.2
	github.com/golang/snappy v0.0.3 // indirect
	github.com/google/uuid v1.1.1
	github.com/oxtoacart/bpool v0.0.0-20190530202638-03653db5a59c
	github.com/pierrec/lz4 v2.6.0+incompatible // indirect
	github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475 // indirect
	github.com/rs/zerolog v1.22.0
	github.com/streadway/amqp v1.0.0
)
