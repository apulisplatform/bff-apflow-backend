package jobscheduler

// 1
// create job request
type CreateJobReq struct {
	Job
}

// create job response
type CreateJobRsp struct {
	JobId    string   `json:"jobId"`
	JobState JobState `json:"jobState"`
}

// 2
// get job response
type GetJobRsp struct {
	*Job
	JobStatus string
	KubeMsg   string
}

// 3
// message queue protocol
type JobMsg struct {
	JobId    string
	JobState JobState
}

// 4
// get log request
type GetLogReq struct {
	PageNum	int `json:"pageNum" form:"pageNum"`
}

// get logs
type GetLogRsp struct {
	Items []string `json:"items"`
	Total	int `json:"total"`
	HasNext bool `json:"hasNext"`
}

// resource quota
type ResourceQuota struct {
	Request  ResourceData `json:"request"`
	Limit 	 ResourceData `json:"limit"`
}

type ResourceData struct {
	// e.g.
	// memory unit：Ei，Pi，Ti，Gi，Mi，Ki
	// cpu unit： 1, 100m（1/10 CPU）
	CPU  		string `json:"cpu"`
	Memory      string `json:"memory"`
	Device 		Device `json:"device"`
}

// device info
type Device struct {

	// e.g.
	//	"deviceType":"npu.huawei.com/NPU",
	//	"deviceNum":"1",
	//	"series":"a910",
	//	"computeType":"huawei_npu"

	// for kubernetes resources request
	DeviceType 	string `json:"deviceType"`
	DeviceNum   string `json:"deviceNum"`

	// for node selector
	Series 		string `json:"series"`
	ComputeType string `json:"computeType"`
}

func (d *Device) EmptyDevice() bool {

	if len(d.DeviceType) == 0 || len(d.DeviceNum) == 0 {
		return true
	}

	return false
}

func (d *Device) GetSeries() string {
	return d.Series
}

func (d *Device) GetComputeType() string {
	return d.ComputeType
}



