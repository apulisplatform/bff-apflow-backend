package jobscheduler


const (
	JOB_STATUS_NAME_UNAPPROVE			=	"unapprove"
	JOB_STATUS_NAME_QUEUEING			=	"queueing"
	JOB_STATUS_NAME_SCHEDULING			=	"scheduling"
	JOB_STATUS_NAME_RUNNING				=	"running"
	JOB_STATUS_NAME_FINISHED			=	"finish"
	JOB_STATUS_NAME_ERROR				=	"error"
	JOB_STATUS_NAME_TERMINATING			=	"terminating"
	JOB_STATUS_NAME_TERMINATED			=	"terminated"
	JOB_STATUS_NAME_UNKOWN				=	"unkown"
)

const (
	MODID_JOB_SCHEUDULER		int	=	108
)

type JobStatus int

const (
	// APP Status
	JOB_STATUS_UNAPPROVE			JobStatus	=	iota
	JOB_STATUS_QUEUEING
	JOB_STATUS_SCHEDULING
	JOB_STATUS_RUNNING
	JOB_STATUS_FINISH

	// based on return code of main process within target pod
	JOB_STATUS_ERROR
	JOB_STATUS_TERMINATING
	JOB_STATUS_TERMINATED
	JOB_STATUS_UNKOWN
)

type ResourceType int
const (
	RESOURCE_TYPE_None 			ResourceType =	iota
	RESOURCE_TYPE_POD
	RESOURCE_TYPE_JOB
	RESOURCE_TYPE_DEPLOYMENT
	RESOURCE_TYPE_CRD
	RESOURCE_TYPE_YAML
	RESOURCE_TYPE_UNKONWN
)


// apply for Job.Ext
type RESOURCE_FIELD_NAME string
const (

	// deployment
	FIELD_REPLICA RESOURCE_FIELD_NAME = "replicas"
	FIELD_PORTS   RESOURCE_FIELD_NAME = "ports"
	FIELD_YAML    RESOURCE_FIELD_NAME = "yaml"
)

// kubernetes labels
const (
	KUBE_LABEL_JOB_ID			string = "job-id"
	KUBE_LABEL_JOB_NAME			string = "job-name"
)

// error code
const (
	ERR_CODE_INVALID_PARAM		int	=	270000001
	ERR_CODE_RESOURCE_EXIST			= 	270000002
	ERR_CODE_RESOURCE_NOT_EXIST		=	270000003

	ERR_CODE_INTERNAL 				=	270001000
)

// service name
const (
	SERVICE_NAME				string	= "job-scheduler.default"
	SERVICE_PORT				int     = 10100
)

// mount point
const (
	PVC_PREFIX					string	=	"pvc://"
	HOSTPATH_PREFIX				string	=	"FILE://"
	HOSTPATH_LOWER_PREFIX		string	=	"file://"

	CM_PREFIX				string	=	"CM://"
	CM_LOWER_PREFIX			string	=	"cm://"
)

// environment
const (
	ENV_KEY_RUN_AS_ROOT			string = "RUN_AS_ROOT"
	ENV_VALUE_RUN_AS_ROOT		string = "true"
)