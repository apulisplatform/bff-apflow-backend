package jobscheduler

import (
	"encoding/json"
)

type DeploymentState struct {
	Replicas int32 `json:"replicas"`
	ReadyReplicas int32 `json:"readyReplicas"`
	AvailableReplicas int32 `json:"availableReplicas"`
	UnavailableReplicas int32 `json:"unavailableReplicas"`
}

type JobState struct {
	Status 		JobStatus `json:"status"`
	Name 		string `json:"name"`
	Msg         string `json:"msg"`
	DeploymentState DeploymentState `json:"deploymentState"`
}

func (j *JobState) GetStatus() JobStatus {
	return j.Status
}

func (j *JobState) GetName() string {
	return j.Name
}

func (j *JobState) GetMsg() string {
	return j.Msg
}

func (j *JobState) ToJson() string {
	data, _ := json.Marshal(j)
	return string(data)
}

func (j *JobState) SetMsg(msg string) {
	j.Msg = msg
}

func (j *JobState) SetDeploymentState(state DeploymentState) {
	j.DeploymentState = state
}

func NewUnapproveJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_UNAPPROVE,
		Name: JOB_STATUS_NAME_UNAPPROVE,
	}
}


func NewSchedulingJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_SCHEDULING,
		Name: JOB_STATUS_NAME_SCHEDULING,
	}
}


func NewQueueJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_QUEUEING,
		Name: JOB_STATUS_NAME_QUEUEING,
	}
}

func NewRunningJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_RUNNING,
		Name: JOB_STATUS_NAME_RUNNING,
	}
}

func NewFinishJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_FINISH,
		Name: JOB_STATUS_NAME_FINISHED,
	}
}

func NewErrorJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_ERROR,
		Name: JOB_STATUS_NAME_ERROR,
	}
}

func NewTerminatingJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_TERMINATING,
		Name: JOB_STATUS_NAME_TERMINATING,
	}
}

func NewTerminatedJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_TERMINATED,
		Name: JOB_STATUS_NAME_TERMINATED,
	}
}

func NewUnkownJobState() *JobState {
	return &JobState{
		Status: JOB_STATUS_UNKOWN,
		Name: JOB_STATUS_NAME_UNKOWN,
	}
}

func ConvertJobState(status string) *JobState  {
	if status == JOB_STATUS_NAME_SCHEDULING {
		return NewSchedulingJobState()
	} else if status == JOB_STATUS_NAME_RUNNING {
		return NewRunningJobState()
	} else if status == JOB_STATUS_NAME_ERROR {
		return NewErrorJobState()
	} else if status == JOB_STATUS_NAME_FINISHED {
		return NewFinishJobState()
	} else {
		return NewUnkownJobState()
	}
}

func AbleToTransitState(src, dst string) bool {

	if src == JOB_STATUS_NAME_SCHEDULING {

		if dst == JOB_STATUS_NAME_SCHEDULING {
			return false
		}
	} else if src == JOB_STATUS_NAME_RUNNING {

		if dst == JOB_STATUS_NAME_QUEUEING ||
			dst == JOB_STATUS_NAME_UNAPPROVE {
			return false
		}

	} else if src == JOB_STATUS_NAME_TERMINATING {

		if dst != JOB_STATUS_NAME_TERMINATED &&
			dst != JOB_STATUS_NAME_FINISHED {
			return false
		}

	} else if src == JOB_STATUS_NAME_TERMINATED {

		if dst != JOB_STATUS_NAME_FINISHED {
			return false
		}

	} else if src  == JOB_STATUS_NAME_FINISHED ||
		src == JOB_STATUS_NAME_ERROR {
		return false
	}

	return true
}