package jobscheduler

import (
	"encoding/json"
	"fmt"
	"net/url"
	"strings"
)

type Job struct {

	ModId    	int `json:"modId"`
	JobId		string `json:"jobId"`
	Owner   	string `json:"owner"`       // user name

	ResType 	 ResourceType `json:"resType"`
	ImageName    string `json:"imageName"`
	Cmd          []string `json:"cmd"`
	Namespace    string `json:"namespace"`

	Labels 		map[string]string `json:"labels"`
	Envs 		map[string]string `json:"envs"`
	MountPoints []MountPoint `json:"mountPoints"`

	ArchType	string `json:"archType"`
	Quota       ResourceQuota `json:"quota"`

	// pre-start
	// *      - exec all scripts
	// empty  - don't exec scripts
	// not empty - exec all scripts, separated by space
	PreStartScripts string `json:"preStartScripts"`

	// depends on resType
	Ext 	    map[RESOURCE_FIELD_NAME]interface{} `json:"ext"`
}

func NewJob() *Job {
	return &Job{
		Cmd:             make([]string, 0),
		Labels:          make(map[string]string),
		Envs:            make(map[string]string),
		MountPoints:     make([]MountPoint, 0),
		Ext:             make(map[RESOURCE_FIELD_NAME]interface{}),
	}
}

func (j *Job) Valid() (bool, string) {

	if j.GetModId() == 0 {
		return false, fmt.Sprintf("invalid module id: %d", j.GetModId())
	}

	resType := j.GetResourceType()
	if resType < RESOURCE_TYPE_POD || resType >= RESOURCE_TYPE_UNKONWN {
		return false, fmt.Sprintf("invalid module id: %d", j.GetModId())
	}

	if len(j.GetImage()) == 0 {
		return false, fmt.Sprintf("empty image name")
	}

	if len(j.GetNamespace()) == 0 {
		return false, fmt.Sprintf("empty namespace ")
	}

	if j.GetResourceType() == RESOURCE_TYPE_DEPLOYMENT {
		_, err := j.GetReplicas()
		if err != nil {
			return false, fmt.Sprintf("%v", err)
		}
	}

	return true, ""
}

func (j *Job) SetModId(modid int) *Job  {
	j.ModId = modid
	return j
}

func (j *Job) GetModId() int {
	return j.ModId
}

func (j *Job) SetJobId(jobId string) *Job  {
	j.JobId = jobId
	return j
}

func (j *Job) GetJobId() string {
	return j.JobId
}

func (j *Job) SetOwner(owner string) *Job  {
	j.Owner = owner
	return j
}

func (j *Job) GetOwner() string {
	return j.Owner
}

func (j *Job) SetResType(resType ResourceType) *Job  {
	j.ResType = resType
	return j
}

func (j *Job) GetResourceType() ResourceType {
	return j.ResType
}

func (j *Job) SetImage(image string) *Job  {
	j.ImageName = image
	return j
}

func (j *Job) GetImage() string {
	return j.ImageName
}

func (j *Job) SetCmd(cmd []string) *Job  {
	j.Cmd = cmd
	return j
}

func (j *Job) GetCmd() []string {
	return j.Cmd
}

func (j *Job) SetNamespace(ns string) *Job  {
	j.Namespace = ns
	return j
}

func (j *Job) GetNamespace() string {
	return j.Namespace
}

func (j *Job) SetLabels(labels map[string]string) *Job  {
	j.Labels = labels
	return j
}

func (j *Job) GetLabels() map[string]string {
	return j.Labels
}

func (j *Job) SetEnvs(envs map[string]string) *Job  {
	j.Envs = envs
	return j
}

func (j *Job) GetEnvs() map[string]string {
	return j.Envs
}

func (j *Job) SetMounts(mounts []MountPoint) *Job  {
	j.MountPoints = mounts
	return j
}

func (j *Job) GetMounts() []MountPoint {
	return j.MountPoints
}

func (j *Job) SetArchType(at string) *Job  {
	j.ArchType = at
	return j
}

func (j *Job) GetArchType() string {
	return j.ArchType
}

func (j *Job) SetQuota(quota ResourceQuota) *Job  {
	j.Quota = quota
	return j
}

func (j *Job) GetQuota() ResourceQuota {
	return j.Quota
}

func (j *Job) SetPreStartScripts(scripts string) *Job  {
	j.PreStartScripts = scripts
	return j
}

func (j *Job) GetPreStartScripts() string  {
	return j.PreStartScripts
}

func (j *Job) ToString() string {
	data, _ := json.Marshal(*j)
	return string(data)
}

func (j *Job) FromString(data string) error {
	return json.Unmarshal([]byte(data), j)
}

func (j *Job) GetReplicas() (int32, error) {

	if j.Ext == nil {
		return 0, fmt.Errorf("replicas field not found!!")
	}

	data, ok := j.Ext[(FIELD_REPLICA)]
	if !ok {
		return 0, fmt.Errorf("replicas field not found!!")
	}

	count := data.(float64)
	return int32(count), nil
}

func (j *Job) SetReplicas(replicas int32) {

	if j.Ext == nil {
		j.Ext = make(map[RESOURCE_FIELD_NAME]interface{})
	}

	j.Ext[(FIELD_REPLICA)] = replicas
	return
}

func (j *Job) GetContainerPorts() ([]ContainerPort, error) {

	if j.Ext == nil {
		return nil, nil
	}

	data, ok := j.Ext[(FIELD_PORTS)]
	if !ok {
		return nil, nil
	}

	//count := data.([]ContainerPort)
	tmp := data.([]interface{})
	if tmp == nil {

		fmt.Println(j.JobId, "cann't convert ports field")
		return nil, nil

	} else {

		ports := make([]ContainerPort, 0)

		for _, port := range tmp {

			jsonString, _ := json.Marshal(port)
			containerPort := ContainerPort{}

			err := json.Unmarshal(jsonString, &containerPort)
			if err != nil {
				fmt.Println(fmt.Sprintf("%s unmashall err(%+v)", j.JobId, err))
				continue
			}

			ports = append(ports, containerPort)
			//if portStruct, ok := port.(ContainerPort); ok {
			//	ports = append(ports, portStruct)
			//} else {
			//	loggers.GetLogger().Error(utils.JobPrint(j.JobId, "cann't convert container port"))
			//	loggers.GetLogger().Error(ports, port)
			//}
		}

		fmt.Println("parsed ports: ", ports)
		return ports, nil
	}
}

func (j *Job) SetContainerPorts(ports []ContainerPort)  {

	if j.Ext == nil {
		j.Ext = make(map[RESOURCE_FIELD_NAME]interface{})
	}

	j.Ext[FIELD_PORTS] = ports
	return
}


func (j *Job) GetYamlContent() (string, error) {

	if j.Ext == nil {
		return "", fmt.Errorf("content field not found!!")
	}

	data, ok := j.Ext[FIELD_YAML]
	if !ok {
		return "", fmt.Errorf("content field not found!!")
	}

	content := data.(string)
	return content, nil
}

func (j *Job) SetYamlContent(content string) {

	if j.Ext == nil {
		j.Ext = make(map[RESOURCE_FIELD_NAME]interface{})
	}

	j.Ext[(FIELD_YAML)] = content
	return
}

func (j *Job) NeedSetupEnv() bool  {
	if len(j.PreStartScripts) > 0 {
		return true
	} else {
		return false
	}
}

func (j *Job) IsKubeJob() bool  {
	if j.ResType == RESOURCE_TYPE_JOB {
		return true
	} else {
		return false
	}
}

func (j *Job) IsKubeDeployment() bool  {
	if j.ResType == RESOURCE_TYPE_DEPLOYMENT {
		return true
	} else {
		return false
	}
}

type MountPoint struct {

	// https://en.wikipedia.org/wiki/File_URI_scheme#Unix

	// example:
	//    hostpath - file:///hostpath
	//    pvc      - pvc://pvc-name/subpath
	//    configmap - cm://configmap-name/subpath

	//    to access /etc/fstab
	//    	file://localhost/	etc/fstab
	//    	file:///etc/fstab
	Path 			string 		`json:"path"`
	ContainerPath   string     	`json:"containerPath"`
	ReadOnly        bool       	`json:"readOnly"`
}

func (m *MountPoint) IsHostPath() bool  {

	if strings.HasPrefix(m.Path, HOSTPATH_PREFIX) ||
		strings.HasPrefix(m.Path, HOSTPATH_LOWER_PREFIX)  {
		return true
	}

	return false
}

func (m *MountPoint) IsPVC() bool  {

	if strings.HasPrefix(m.Path, PVC_PREFIX) {
		return true
	}

	return false
}

func (m *MountPoint) GetPVC() (name, subPath string)  {

	url, err := url.Parse(m.Path)
	if err != nil {
		fmt.Printf("parse pvc err: %v", err)
		return "", ""
	}

	return url.Host, strings.TrimPrefix(url.Path, "/")
}

func (m *MountPoint) IsConfigMap() bool  {

	if strings.HasPrefix(m.Path, CM_PREFIX) ||
		strings.HasPrefix(m.Path, CM_LOWER_PREFIX)  {
		return true
	}

	return false
}

func (m *MountPoint) GetConfigMap() (name, subPath string) {

	url, err := url.Parse(m.Path)
	if err != nil {
		fmt.Printf("parse configmap err: %v", err)
		return "", ""
	}

	return url.Host, strings.TrimPrefix(url.Path, "/")
}

func (m *MountPoint) GetHostPath() (path string)  {

	if strings.HasPrefix(m.Path, HOSTPATH_PREFIX) {
		return strings.TrimPrefix(m.Path, HOSTPATH_PREFIX)
	} else {
		return strings.TrimPrefix(m.Path, HOSTPATH_LOWER_PREFIX)
	}
}


type JobContext struct {
	JobData       *Job
	CurrentState  *JobState
	KubeResObject KubeResObject // either of batchv1.Job、Pod、deployment
}

type KubeResObject interface {

	GetJobState() *JobState
	GetKubeMsg() string

	// return job-id from labels
	// return object name if no job-id label was set
	GetJobId() string
}

type ContainerPort struct {
	Port     int    `json:"port"`
	TargetPort int `json:"targetPort"`
	PortName string `json:"portName"`
	ServiceName string `json:"serviceName"`
}
