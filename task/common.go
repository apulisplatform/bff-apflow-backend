package task

import (
	"apflow/pkg/common"

	"github.com/apulis/go-business/pkg/jobscheduler"
)

var Job2TaskStatus = map[jobscheduler.JobStatus]string{
	jobscheduler.JOB_STATUS_UNAPPROVE:   common.TASK_STATUS_SCHEDULING,
	jobscheduler.JOB_STATUS_QUEUEING:    common.TASK_STATUS_SCHEDULING,
	jobscheduler.JOB_STATUS_SCHEDULING:  common.TASK_STATUS_SCHEDULING,
	jobscheduler.JOB_STATUS_RUNNING:     common.TASK_STATUS_RUNNING,
	jobscheduler.JOB_STATUS_FINISH:      common.TASK_STATUS_ERROR,
	jobscheduler.JOB_STATUS_ERROR:       common.TASK_STATUS_ERROR,
	jobscheduler.JOB_STATUS_UNKOWN:      common.TASK_STATUS_ERROR,
	jobscheduler.JOB_STATUS_TERMINATING: common.TASK_STATUS_TERMINATING,
	jobscheduler.JOB_STATUS_TERMINATED:  common.TASK_STATUS_STOP,
}
