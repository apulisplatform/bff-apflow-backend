package task

import (
	aipserver "apflow/apiserver"
	"apflow/dao"
	"apflow/pkg/common"
	"apflow/pkg/configs"
	"apflow/pkg/database"
	"apflow/pkg/loggers"
	"apflow/pkg/protocol"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/apulis/go-business/pkg/jobscheduler"
	"github.com/apulis/sdk/go-utils/broker"
	"github.com/apulis/sdk/go-utils/broker/rabbitmq"
)

var logger = loggers.LogInstance()

func MqTask() {
	url := fmt.Sprintf("amqp://%s:%s@%s:%s/",
		configs.Config.Rabbitmq.UserName,
		configs.Config.Rabbitmq.Password,
		configs.Config.Rabbitmq.Host,
		configs.Config.Rabbitmq.Port)
	logger.Infoln("rabbitmq url:", url)
	rabbitMQBroker := rabbitmq.NewBroker(
		broker.Addrs(url),
	)

	if err := rabbitMQBroker.Connect(); err != nil {
		panic(err)
	}
	logger.Infoln("rabbitmq connect success!")

	_, err := rabbitMQBroker.Subscribe(
		configs.Config.Relevant.MqSubMod,
		RabbitMQMsgHandler,
		rabbitmq.DurableQueue(),
	)
	if err != nil {
		panic(err)
	}

	_, err = rabbitMQBroker.Subscribe(
		"centre_inference_notify",
		WorkShopHandler,
		rabbitmq.DurableQueue(),
		broker.Queue("centre_inference_notify"),
	)
	if err != nil {
		panic(err)
	}
}

func RabbitMQMsgHandler(event broker.Event) error {
	var jobMsg jobscheduler.JobMsg
	logger.Errorln("----------------------------------")
	logger.Errorln(string(event.Message().Body))
	err := json.Unmarshal(event.Message().Body, &jobMsg)
	if err != nil {
		return err
	}
	logger.Info(fmt.Sprintf("recv rabbitmq message: %+v \n", jobMsg))
	err = HandleJobMsg(jobMsg)
	if err != nil {
		return err
	}
	return nil
}

func HandleJobMsg(jobMsg jobscheduler.JobMsg) error {

	// 获取状态
	jobStatus := jobMsg.JobState.GetStatus()
	taskStatus, ok := Job2TaskStatus[jobStatus]
	if !ok {
		return nil
	}

	// 获取多个副本状态
	okPodNum := jobMsg.JobState.DeploymentState.ReadyReplicas
	PodNum := jobMsg.JobState.DeploymentState.Replicas
	repStatus := strconv.Itoa(int(okPodNum)) + "/" + strconv.Itoa(int(PodNum))
	logger.Infoln("pod status:", repStatus)
	tx := database.Db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	task := dao.ApflowTask{
		ServiceId:      jobMsg.JobId,
		Status:         taskStatus,
		ReplicasStatus: repStatus,
		Events:         jobMsg.JobState.GetMsg(),
	}
	err := task.UpdateStatusByServiceId(tx)
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit().Error
}

type StudioModelTransformRsp struct {
	ModelId        int    `json:"modelId"`
	ServiceId      string `json:"serviceId"`
	ModelVersionId int    `json:"modelVersionId"`
	Success        bool   `json:"success"`
}

type ModelTransformStatus struct {
}

func WorkShopHandler(event broker.Event) error {
	var jobMsg StudioModelTransformRsp

	logger.Errorln("----------------------------------")
	logger.Errorln(string(event.Message().Body))
	err := json.Unmarshal(event.Message().Body, &jobMsg)
	if err != nil {
		return err
	}
	logger.Info(fmt.Sprintf("recv workshop rabbitmq message: %+v \n", jobMsg))
	err = HandleWS(jobMsg)
	if err != nil {
		return err
	}
	return nil
}

func HandleWS(jobMsg StudioModelTransformRsp) error {
	var taskStatus string
	// 模型转换判断是否成功
	if jobMsg.Success {
		taskStatus = common.TASK_STATUS_MODEL_TRANSFORM_OK
	} else {
		taskStatus = common.TASK_STATUS_MODEL_TRANSFORM_FAILED
	}

	tx := database.Db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	task := dao.ApflowTask{
		ServiceId: jobMsg.ServiceId,
		Status:    taskStatus,
	}
	err := task.UpdateModelStatusByServiceId(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	item, err := task.GetByServiceId(tx)
	if err != nil {
		tx.Rollback()
		return err
	}

	// 成功去启动服务
	if jobMsg.Success {
		userInfo := &protocol.UserInfoAAA{
			UserName: item.Caller,
		}
		logger.Infoln("start to apflow")
		err = aipserver.StartApflow(item, userInfo, tx)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit().Error
}
