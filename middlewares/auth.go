package middlewares

import (
	"apflow/pkg/configs"
	"fmt"

	"github.com/apulis/go-business/pkg/jwt"
	"github.com/gin-gonic/gin"
)

func AuthMiddleware() gin.HandlerFunc {
	JwtConfig := configs.Config.Relevant.Jwt

	return jwt.NewJwtAuthN(
		jwt.SigningAlgorithm(JwtConfig.SignAlgorithm),
		jwt.SecretKey([]byte(JwtConfig.SecretKey)),
		jwt.PublicKey(JwtConfig.PublicKey),
	).Middleware()
}

func SetHeadersWithContextKeys() gin.HandlerFunc {
	return func(c *gin.Context) {
		for key, value := range c.Keys {
			c.Request.Header.Set(key, fmt.Sprintf("%+v", value))
			fmt.Printf("%s:%v\n", key, value)
		}
	}
}
