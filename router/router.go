package router

import (
	"apflow/controller"
	"apflow/dto"
	"apflow/middlewares"
	"apflow/pkg/configs"
	"apflow/pkg/loggers"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

// @title ApulisInference API
// @version alpha
// @description ApulisInference server.
func NewRouter(config configs.AppConfig) *gin.Engine {
	r := gin.New()

	r.GET("/swagger/*any", ginSwagger.DisablingWrapHandler(swaggerFiles.Handler, "DISABLE_SWAGGER"))
	r.GET("/health", dto.HandleProb)

	r.Use(cors.Default())
	r.NoMethod(dto.HandleNotFound)
	r.NoRoute(dto.HandleNotFound)
	r.Use(loggers.GinLogger(logger))
	r.Use(gin.Recovery())

	r.Use(middlewares.AuthMiddleware())
	r.Use(middlewares.SetHeadersWithContextKeys())

	controller.InferenceTaskRouter(r)
	return r
}
