module apflow

go 1.16

replace github.com/apulis/go-business v0.0.0 => ./deps/go-business

replace github.com/apulis/sdk/go-utils => ./deps/go-utils

require (
	github.com/apulis/go-business v0.0.0
	github.com/apulis/sdk/go-utils v0.0.0-00010101000000-000000000000
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.3
	github.com/go-playground/validator/v10 v10.4.1
	github.com/jinzhu/gorm v1.9.16
	github.com/mitchellh/mapstructure v1.4.1
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.1
	github.com/swaggo/gin-swagger v1.3.0
	gopkg.in/yaml.v2 v2.4.0
)
