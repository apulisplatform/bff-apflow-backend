package dto

type CreateInferenceTaskReq struct {
	Name       string       `json:"name" comment:"任务名称" validate:"required,name-checker"`
	TaskType   string       `json:"taskType" comment:"任务类型"`
	Describe   string       `json:"describe" comment:"任务描述"`
	Models     []Model      `json:"models" comment:"模型" validate:"required"`
	DeviceType string       `json:"deviceType" comment:"设备类型" validate:"required,oneof=GPU CPU NPU"`
	Resource   ResourceConf `json:"resource" comment:"规格"`
	Replicas   int32        `json:"replicas" comment:"任务描述"`
	Envs       EnvMap       `json:"envs" comment:"环境变量"`
}

type ResourceConf struct {
	Arch      string  `json:"arch" comment:"架构" validate:"required,oneof=ARM AMD"`
	Cpu       float64 `json:"cpu" comment:"cpu" validate:"required"`
	Mem       int64   `json:"mem" comment:"mem" validate:"required"`
	TypeSpec  string  `json:"typeSpec" comment:"型号"`
	DeviceNum int64   `json:"deviceNum" comment:"设备数量"`
}

type EnvMap map[string]string

type Model struct {
	Name      string `json:"name" validate:"required"`
	ID        int64  `json:"id" validate:"required"`
	Version   string `json:"version" validate:"required"`
	VersionId int64  `json:"versionId" validate:"required"`
}

type CreateInferenceTaskRsp struct {
	ID int64 `json:"id"`
}

type GetInferenceTaskDetailReq struct{}

type GetInferenceTaskDetailRsp struct {
	OutApflowTaskItem
}

// 获取列表
type GetInferenceTaskListReq struct {
	PageNum  int64  `form:"pageNum" validate:"required"`
	PageSize int64  `form:"pageSize" validate:"required"`
	TaskType string `form:"taskType"`
	Name     string `form:"name"`
	Sort     string `form:"sort"`
	Status   string `form:"status"`
}

type GetInferenceTaskListRsp struct {
	Items []OutApflowTaskItem `json:"items"`
	Total int64               `json:"total"`
}

type OutApflowTaskItem struct {
	ID             int64   `json:"id"`
	OrgId          int64   `json:"orgId"`
	GroupId        int64   `json:"groupId"`
	UserId         int64   `json:"userId"`
	ServiceId      string  `json:"serviceId"`
	UserName       string  `json:"userName"`
	Name           string  `json:"name"`
	Describe       string  `json:"describe"`
	Replicas       int32   `json:"replicas"`
	Caller         string  `json:"caller"`
	StartTime      int64   `json:"startTime"`
	Url            string  `json:"url"`
	Status         string  `json:"status"`
	ReplicasStatus string  `json:"replicasStatus"`
	TaskType       string  `json:"taskType"`
	Events         string  `json:"events"`
	Arch           string  `json:"arch"`
	DeviceType     string  `json:"deviceType"`
	DeviceNum      int64   `json:"deviceNum"`
	Cpu            float64 `json:"cpu"`
	Mem            int64   `json:"mem"`
	TypeSpec       string  `json:"typeSpec"`
	Models         []Model `json:"models"`
	Envs           string  `json:"envs"`
	CreatedAt      int64   `json:"createdAt"`
	UpdatedAt      int64   `json:"updatedAt"`
	RunTime        int64   `json:"runTime"`
}

type DelInferenceTaskReq struct{}

type DelInferenceTaskRsp struct{}

type PutInferenceTaskReq struct {
	CreateInferenceTaskReq
}

type PutInferenceTaskRsp struct{}

type OpInferenceTaskReq struct {
	Cmd string `json:"cmd" validate:"required"`
}

type OpInferenceTaskRsp struct{}

type GetReplicasReq struct{}

type GetReplicasRsp struct {
	MaxReplicas int32 `json:"maxReplicas"`
}

// 日志下载
type GetLogDownloadReq struct{}

type GetLogDownloadRsp struct {
	Downloadlink string `json:"downloadlink"`
}

// 预测参数
type PredictInfoReq struct{}

type PredictInfoRsp struct {
	Center      map[string]interface{} `json:"center"`
	PredictHost string                 `json:"predictHost"`
	Status      string                 `json:"status"`
	Url         string                 `json:"url"`
	ProxyUrl    string                 `json:"proxyUrl"`
}

// 适配APSC单独接口
type APSCInferenceTaskListReq struct {
	PageNum     int64  `form:"pageNum" validate:"required"`
	PageSize    int64  `form:"pageSize" validate:"required"`
	JobType     string `form:"jobType"`
	JobStatus   string `form:"jobStatus"`
	OrgId       int64  `form:"orgId"`
	UserGroupId int64  `form:"userGroupId"`
	JobId       string `form:"jobId"`
	ProjectName string `form:"projectName"`
	UserName    string `form:"userName"`
	Sort        string `form:"sort"`
	Filter      string `form:"filter"`
	StartTime   int64  `form:"startTime"`
	EndTime     int64  `form:"endTime"`
}

type APSCInferenceTaskListRsp struct {
	Items    []APSCOutApflowTaskItem `json:"items"`
	PageNum  int64                   `form:"pageNum"`
	PageSize int64                   `form:"pageSize"`
	Total    int64                   `json:"total"`
}

type APSCStaticInfoReq struct{}

type APSCStaticInfoRsp struct {
	TaskStatus []APSCTaskStatus `json:"taskStatus"`
	TaskTypes  []APSCTaskType   `json:"taskTypes"`
}

// type APSCTaskStatus struct {
// 	EN string `json:"en"`
// 	ZH string `json:"zh"`
// }

type APSCTaskStatus struct {
	Label string `json:"lable"`
	Value string `json:"value"`
}

type APSCTaskType struct {
	Label string `json:"lable"`
	Value string `json:"value"`
}

type APSCOutApflowTaskItem struct {
	ID          int64      `json:"id"`
	Status      string     `json:"status"`
	ProjectName string     `json:"projectName"`
	Type        string     `json:"type"`
	OwnerUser   string     `json:"ownerUser"`
	OwnerOrg    string     `json:"ownerOrg"`
	UserGroup   string     `json:"userGroup"`
	CreatedAt   int64      `json:"createdAt"`
	Duration    int64      `json:"duration"`
	Resources   []Resource `json:"resources"`
}

type Resource struct {
	Model  string `json:"model"`
	Count  int64  `json:"count"`
	Amount int64  `json:"amount"`
}
