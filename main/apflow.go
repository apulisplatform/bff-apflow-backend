package main

import (
	"apflow/pkg/configs"
	"apflow/pkg/database"
	"apflow/pkg/loggers"
	"apflow/pkg/validators"
	"apflow/remote"
	"apflow/router"
	"apflow/task"
	"flag"
	"fmt"
	"os"
	"os/signal"
	"syscall"
)

var logger = loggers.LogInstance()

var (
	configFile = flag.String("config", "/etc/bff-apflow-backend/conf/config.yaml", "input config file like ../conf/dev/")
)

func main() {
	logger.Infof("Bff Apflow Backend Start! PID = %d", os.Getpid())
	flag.Parse()
	fmt.Println("config file path: ", *configFile)
	configs.InitConfig(*configFile, &configs.Config)
	loggers.InitLogger(configs.Config)
	database.InitDatabase(configs.Config)
	database.InitTables()
	validators.InitValidator()
	task.MqTask()

	err := remote.InitEndpointPolicy()
	if err != nil {
		panic(err)
	}
	// start api server
	srv := router.StartApiServer(configs.Config)

	// quit when signal notifys
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit

	router.StopApiServer(srv)
	logger.Infof("Bff Apflow Backend Exit!")
}
